/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2024. MIT licensed.
 */$('.navbar-toggler').on('click', function () {
  $(this).toggleClass('active');
  $('.navbar-collapse').toggleClass('show');
});
$('.navbar-collapse .close').on('click', function () {
  $('.navbar-collapse').removeClass('show');
});
// active navbar of page current
var urlcurrent = window.location.pathname;
$(".navbar-nav li a[href$='" + urlcurrent + "'],.nav-category li a[href$='" + urlcurrent + "']").addClass('active');
$(window).on("load", function (e) {
  $(".navbar-nav .sub-menu").parent("li").append("<span class='show-menu'></span>");
});
$('.navbar-nav > li').click(function () {
  $(this).toggleClass('active');
});
$('.btn-search').on('click', function () {
  $('.form-search input').focus();
  $('.form-search').toggleClass('show');
});

//scroll effect
$.fn.isInViewport = function () {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();
  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();
  return elementBottom > viewportTop && elementTop < viewportBottom;
};
$(window).on('resize scroll load', function () {
  $('.fadeup').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('fadeInUp').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
  $('.fadein').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('fadeIn').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
  $('.zoomin').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('zoomIn').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
  $('.fadeinleft').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('fadeInLeft').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
  $('.fadeinright').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('fadeInRight').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
  $('.kzt').each(function () {
    if ($(this).isInViewport()) {}
  });
});